/*package FunctionalTestsSelenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class nr1registerTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    System.setProperty("webdriver.chrome.driver", "chromedriver");
    driver = new ChromeDriver();
    baseUrl = "http://pc-sensez:21648/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testRegister() throws Exception {
    driver.get(baseUrl + "/TaskManager-1.0-SNAPSHOT/login.xhtml");
    driver.findElement(By.id("loginForm:j_idt29")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
    driver.findElement(By.id("logonForm:username")).clear();
    driver.findElement(By.id("logonForm:username")).sendKeys("admin");
    driver.findElement(By.id("logonForm:password")).clear();
    driver.findElement(By.id("logonForm:password")).sendKeys("admin");
    driver.findElement(By.id("logonForm:repeatPassword")).clear();
    driver.findElement(By.id("logonForm:repeatPassword")).sendKeys("admin");
    driver.findElement(By.id("logonForm:email")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:email")).sendKeys("admin@ua.pt");
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:j_idt24")).click();
    driver.findElement(By.id("logonForm:j_idt27")).click();
    driver.findElement(By.id("loginForm:j_idt29")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
    driver.findElement(By.id("logonForm:username")).clear();
    driver.findElement(By.id("logonForm:username")).sendKeys("joana");
    driver.findElement(By.id("logonForm:password")).clear();
    driver.findElement(By.id("logonForm:password")).sendKeys("joana");
    driver.findElement(By.id("logonForm:repeatPassword")).clear();
    driver.findElement(By.id("logonForm:repeatPassword")).sendKeys("joana");
    driver.findElement(By.id("logonForm:email")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:email")).sendKeys("joana@ua.pt");
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:j_idt24")).click();
    driver.findElement(By.id("logonForm:j_idt27")).click();
    driver.findElement(By.id("loginForm:j_idt29")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
    driver.findElement(By.id("logonForm:username")).clear();
    driver.findElement(By.id("logonForm:username")).sendKeys("maria");
    driver.findElement(By.id("logonForm:password")).clear();
    driver.findElement(By.id("logonForm:password")).sendKeys("maria");
    driver.findElement(By.id("logonForm:repeatPassword")).clear();
    driver.findElement(By.id("logonForm:repeatPassword")).sendKeys("maria");
    driver.findElement(By.id("logonForm:email")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:email")).sendKeys("maria@ua.pt");
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:j_idt24")).click();
    driver.findElement(By.id("logonForm:j_idt27")).click();
    Thread.sleep(500);
    driver.get("http://pc-sensez:21648/TaskManager-1.0-SNAPSHOT/rest/AppUser/");
    Thread.sleep(500);
    assertEquals("[{\"userId\":\"1\", \"userName\":\"admin\", \"email\":\"admin@ua.pt\", \"password\":\"admin\"}, {\"userId\":\"2\", \"userName\":\"joana\", \"email\":\"joana@ua.pt\", \"password\":\"joana\"}, {\"userId\":\"3\", \"userName\":\"maria\", \"email\":\"maria@ua.pt\", \"password\":\"maria\"}]", driver.findElement(By.cssSelector("html")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
*/