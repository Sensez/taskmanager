/*package FunctionalTestsSelenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class or10sharedTaskTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    System.setProperty("webdriver.chrome.driver", "chromedriver");
    driver = new ChromeDriver();
    baseUrl = "http://pc-sensez:21648/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testSharedTask() throws Exception {
    driver.get(baseUrl + "/TaskManager-1.0-SNAPSHOT/login.xhtml");
    driver.findElement(By.id("loginForm:password")).clear();
    driver.findElement(By.id("loginForm:password")).sendKeys("admin");
    driver.findElement(By.id("loginForm:username")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("loginForm:username")).sendKeys("admin@ua.pt");
    driver.findElement(By.id("loginForm:password")).click();
    driver.findElement(By.id("loginForm:j_idt23")).click();
    driver.findElement(By.linkText("Consult Tasks")).click();
    driver.findElement(By.id("j_idt5:j_idt6:0:j_idt17")).click();
    driver.findElement(By.linkText("Consult Tasks")).click();
    driver.findElement(By.id("j_idt5:j_idt6:0:j_idt17")).click();
    driver.findElement(By.linkText("Consult Tasks")).click();
    driver.findElement(By.id("j_idt5:j_idt6:0:j_idt17")).click();
    driver.findElement(By.linkText("Create Shareable Task")).click();
    driver.findElement(By.id("logonForm:username")).clear();
    driver.findElement(By.id("logonForm:username")).sendKeys("Supermercado");
    driver.findElement(By.id("logonForm:description")).clear();
    driver.findElement(By.id("logonForm:description")).sendKeys("Ir ao Supermercado");
    driver.findElement(By.xpath("//div[@id='logonForm:rating']/div[3]/a")).click();
    driver.findElement(By.xpath("//div[@id='logonForm:rating']/div[6]/a")).click();
    driver.findElement(By.id("logonForm:users")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("logonForm:users")).sendKeys("joana@ua.pt\nmaria@ua.pt");
    driver.findElement(By.id("logonForm:j_idt11")).click();
    driver.findElement(By.xpath("//div[@id='j_idt5:j_idt7']/ul/li[5]/a/span[2]")).click();
    assertEquals("Ir ao Supermercado", driver.findElement(By.xpath("//div[@id='j_idt5:j_idt6:0:j_idt9']/table/tbody/tr[2]/td[2]/span")).getText());
    driver.findElement(By.id("j_idt5:j_idt6:0:j_idt17")).click();
    driver.get(baseUrl + "/TaskManager-1.0-SNAPSHOT/login.xhtml");
    driver.findElement(By.id("loginForm:password")).clear();
    driver.findElement(By.id("loginForm:password")).sendKeys("admin");
    driver.findElement(By.id("loginForm:username")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("loginForm:username")).sendKeys("maria@ua.pt");
    driver.findElement(By.id("loginForm:password")).clear();
    driver.findElement(By.id("loginForm:password")).sendKeys("maria");
    driver.findElement(By.id("loginForm:j_idt23")).click();
    driver.findElement(By.linkText("Consult Tasks")).click();
    assertEquals("Ir ao Supermercado", driver.findElement(By.xpath("//div[@id='j_idt5:j_idt6:0:j_idt9']/table/tbody/tr[2]/td[2]/span")).getText());
    driver.findElement(By.id("j_idt5:j_idt6:0:j_idt17")).click();
    driver.get(baseUrl + "/TaskManager-1.0-SNAPSHOT/login.xhtml");
    driver.findElement(By.id("loginForm:username")).clear();
    Thread.sleep(500);
    driver.findElement(By.id("loginForm:username")).sendKeys("joana@ua.pt");
    driver.findElement(By.id("loginForm:password")).clear();
    driver.findElement(By.id("loginForm:password")).sendKeys("joana");
    driver.findElement(By.id("loginForm:j_idt23")).click();
    driver.findElement(By.linkText("Consult Tasks")).click();
    assertEquals("Ir ao Supermercado", driver.findElement(By.xpath("//div[@id='j_idt5:j_idt6:0:j_idt9']/table/tbody/tr[2]/td[2]/span")).getText());
    driver.findElement(By.id("j_idt5:j_idt6:0:j_idt17")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}*/