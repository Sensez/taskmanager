
package IntegrationTests;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClientIntegrationTest {
    private Client cli;
    private WebTarget target;
    private String baseURL;
    
    @Before
    public void setUp() {
        this.cli = ClientBuilder.newClient();
        baseURL = "http://pc-sensez:21648/TaskManager-1.0-SNAPSHOT/rest/AppUser/";
        this.target = cli.target(baseURL);
    }
    
    @Test
    public void apostUser() {
        Response responsePost = target.path("/admin/admin@ua.pt/admin").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(""));
        assertTrue(responsePost.getStatus() == 201 || responsePost.getStatus() == 204);
        JsonObject emailClient = target.path("admin@ua.pt").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals("admin", emailClient.getString("userName"));
        
        Response responsePost2 = target.path("/test/test@ua.pt/test").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(""));
        assertTrue(responsePost2.getStatus() == 201 || responsePost2.getStatus() == 204);
        JsonObject emailClient2 = target.path("test@ua.pt").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals("test", emailClient2.getString("userName"));
    }
    
    
    @Test
    public void btestFetchAllCLients(){
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        assertFalse(allTodos.isEmpty());
        
        JsonObject firstClient = allTodos.getJsonObject(0);
        assertTrue(firstClient.getString("userName").equals("admin"));
        
        JsonObject emailClient = target.path("admin@ua.pt").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals(firstClient, emailClient);
    }
    
    @Test
    public void cdeleteUser(){
        Response responseDelete = target.path("/test@ua.pt").request().delete();
        assertTrue(responseDelete.getStatus() == 200 || responseDelete.getStatus() == 204);
    }
    
    @After
    public void tearDown() {
        cli.close();
    }
}