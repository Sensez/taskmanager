
package IntegrationTests;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TasksIntegrationTest {
    private Client cli;
    private WebTarget target;
    private String baseURL;
    
    @Before
    public void setUp() {
        this.cli = ClientBuilder.newClient();
        baseURL = "http://pc-sensez:21648/TaskManager-1.0-SNAPSHOT/rest/Tasks/";
        this.target = cli.target(baseURL);
    }
    
    @Test
    public void apostTask() {
        Response responsePost = target.path("/Supermercado/IraoSupermercado/5/admin@ua.pt").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(""));
        assertTrue(responsePost.getStatus() == 201 || responsePost.getStatus() == 204);
        JsonObject createdTask = target.path("Supermercado").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertEquals("IraoSupermercado", createdTask.getString("description"));
    }
    
    @Test
    public void bgetByUser() {
        Response response = target.path("/admin@ua.pt/admin").request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        assertFalse(allTodos.isEmpty());
        
        JsonObject firstClient = allTodos.getJsonObject(0);
        assertTrue(firstClient.getString("title").equals("Supermercado"));
    }
    
    @Test
    public void ctestFetchAllTasks(){
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        assertFalse(allTodos.isEmpty());
        
        JsonObject firstTask = allTodos.getJsonObject(0);
        assertTrue(firstTask.getString("title").equals("Supermercado"));
    }
    
    
    @Test
    public void ddeleteTask(){
        Response responseDelete = target.path("/Supermercado").request().delete();
        assertTrue(responseDelete.getStatus() == 200 || responseDelete.getStatus() == 204);
    }
}
