/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitTestsJunit;

import entities.Task;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sensez
 */
public class TaskTest {
    
    private Task task;
    
    public TaskTest(){
        task = new Task();
    }
    
    
    @Before
    public void setUp() {
        task.setdescription("Doing a random stuff");
        task.setpriority(5);
        task.settitle("Stuff");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreate(){
        Task ts = new Task("stuff", "stuff", 6);
    }
    
    @After
    public void release(){
        task = null;
    }
}
