/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitTestsJunit;

import entities.AppUser;
import entities.Task;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author sensez
 */
public class AppUserTest {
    
    private AppUser user;
    private AppUser user2;
    private Task task;
    
    public AppUserTest(){
        user = new AppUser();
        task = new Task();
        user2 = new AppUser();
    }
    
    @Before
    public void setUp() {
        user.setemail("admin@ua.pt");
        user.setpassword("admin");
        user.setuserName("admin");
        task.setdescription("doing stuffs");
        task.setpriority(5);
        task.settitle("Stuff");
    }
    
    @Test
    public void addTest(){
        int before = user.TaskSize();
        user.addTask(task);
        int after = user.TaskSize();
        assertTrue("Task not added", after>before);
    }
    
    @Test
    public void removeTest(){
        user.addTask(task);
        int before = user.TaskSize();
        user.removeTask(task);
        int after = user.TaskSize();
        assertTrue("Task not deleted", before>after);
    }
    
    @After
    public void release(){
        user = null;
        user2 = null;
        task = null;
    }
}
