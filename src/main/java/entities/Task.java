/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
public class Task {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long taskId;
    private String title;
    private String description;
    private Integer priority;
    
    private static final long serialVersionUID = 1L;
    public static final int LOW_PRIORITY = 3;
    public static final int MEDIUM_PRIORITY = 2;
    public static final int HIGH_PRIORITY = 1;

    public Task() {
    }

    public Task(String title, String description, Integer priority) {
        this.title = title;
        this.description = description;
        if(priority > 5 || priority < 0)
            throw new IllegalArgumentException("Invalid Priority!");
        this.priority = priority;
    }

    public String gettitle() {
        return title;
    }

    
    public String getdescription() {
        return description;
    }

    public int getpriority() {
        return priority;
    }
    
    public Long gettaskId() {
        return taskId;
    }

    public void settitle(String title) {
        this.title = title;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public void setpriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "{\"taskId\":\"" + gettaskId() + "\", \"title\":\"" + gettitle() + "\", \"description\":\"" + getdescription() + "\"," + " \"priority\":\"" + getpriority()+ "\"}"; 
        }   
    }

