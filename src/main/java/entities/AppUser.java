/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private String userName;
    private String email;
    private String password;
    @OneToMany(fetch=FetchType.EAGER)
    private List<Task> tasks = new ArrayList<>();
    private static final long serialVersionUID = 1L;

    public AppUser(){   
    }
    
    public AppUser(String userName, String email, String password) {
        this.userName = userName;
        this.email = email;
        this.password = password;
    }

    public void setuserName(String userName) {
        this.userName = userName;
    }
    
    public void setpassword(String password) {
        this.password = password;
    }
        
    public void setemail(String email) {
        this.email = email;
    }

    public Long getuserId() {
        return userId;
    }

    public String getuserName() {
        return userName;
    }

    public String getemail() {
        return email;
    }
    
    public String getpassword() {
        return password;
    }
    
    public void addTask(Task task){
        tasks.add(task);
    }
    
    public void removeTask(Task task){
        for(int i=0; i<tasks.size(); i++){
            Task t = tasks.get(i);
            if(t.getdescription().equals(task.getdescription())){
                tasks.remove(i);
            }
        }
    }
    
    public int TaskSize(){
        return tasks.size();
    }
    
    public List<Task> getAllTasks(){
        return tasks;
    }

    @Override
    public String toString() {
        return "{\"userId\":\"" + getuserId() + "\", \"userName\":\"" + getuserName() + "\", \"email\":\"" + getemail() + "\"," + " \"password\":\"" + getpassword() + "\"}"; }   
}
 