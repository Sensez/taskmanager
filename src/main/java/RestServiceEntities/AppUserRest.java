/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestServiceEntities;

import entities.AppUser;
import Facades.AppUserFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
/**
 *
 * @author sensez
 */


@Path("AppUser")
public class AppUserRest {

    @EJB
    private AppUserFacade facade = new AppUserFacade();

    @GET
    @Path("/")
    public Response findAll() {
        List<AppUser> clientList = facade.findAll();
        String output = clientList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{userName}/{email}/{password}")
    public Response add(@PathParam("userName") String userName, @PathParam("email") String email, @PathParam("password") String password) {
        if (facade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":/Duplicated User\"}").build();
        }
        AppUser client = new AppUser();
        client.setuserName(userName);
        client.setemail(email);
        client.setpassword(password);
        facade.create(client);
        return Response.status(201).entity("{\"State\":/OK\"}").build();
    }

    @GET
    @Path("/{email}")
    public Response find(@PathParam("email") String email) {

        if (!facade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":/Client does not exist\"}").build();
        }
        AppUser desiredClient = facade.getAppUserByEmail(email);
        String output = desiredClient.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{email}")
    public Response delete(@PathParam("email") String email) {

        if (!facade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":/Client does not exist\"}").build();
        }
        AppUser desiredClient = facade.getAppUserByEmail(email);
        facade.remove(desiredClient);
        return Response.status(200).entity("{\"State\":/OK\"}").build();
    }
}
