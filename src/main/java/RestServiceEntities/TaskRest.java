/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestServiceEntities;

import entities.AppUser;
import Facades.AppUserFacade;
import Facades.TaskFacade;
import entities.Task;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
/**
 *
 * @author sensez
 */


@Path("Tasks")
public class TaskRest {


    @EJB
    private TaskFacade taskfacade = new TaskFacade();
    
    @EJB
    private AppUserFacade facade = new AppUserFacade();
    
    @GET
    @Path("/")
    public Response findAll() {
        List<Task> tasksList = taskfacade.findAll();
        String output = tasksList.toString();
        return Response.status(200).entity(output).build();
    }
    
    @GET
    @Path("/{userEmail}/{userName}")
    public Response findByEmail(@PathParam("userEmail") String userEmail, @PathParam("userName") String userName) {
        AppUser user = facade.getAppUserByEmail(userEmail);
        List<Task> tasksList = user.getAllTasks();
        String output = tasksList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{title}/{description}/{priority}/{userEmail}")
    public Response add(@PathParam("title") String title, @PathParam("description") String description, @PathParam("priority") Integer priority, @PathParam("userEmail") String userEmail) {
        if (taskfacade.taskExists(title) ) {
            return Response.status(204).entity("{\"State\":/Duplicated Task\"}").build();
        }
        else if(!facade.clientExists(userEmail)){
            return Response.status(204).entity("{\"State\":/User Doesnt Exist\"}").build();
        }
        Task task = new Task();
        task.settitle(title);
        task.setdescription(description);
        task.setpriority(priority);
        taskfacade.create(task);
        AppUser user = facade.getAppUserByEmail(userEmail);
        user.addTask(task);
        facade.edit(user);
        return Response.status(201).entity("{\"State\":/OK\"}").build();
    }

    @GET
    @Path("/{title}")
    public Response find(@PathParam("title") String title) {

        if (!taskfacade.taskExists(title)) {
            return Response.status(204).entity("{\"State\":/Task does not exist\"}").build();
        }
        Task desiredTask = taskfacade.getTaskByTitle(title);
        String output = desiredTask.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{title}")
    public Response delete(@PathParam("title") String title) {

        if (!taskfacade.taskExists(title)) {
            return Response.status(204).entity("{\"State\":/Task does not exist\"}").build();
        }
        Task desiredtask = taskfacade.getTaskByTitle(title);
        List<AppUser> usersList = facade.findAll();
        for(int i=0; i<usersList.size(); i++){
            AppUser user = usersList.get(i);
            List<Task> userTasks = user.getAllTasks();
            for(int j=0; j<userTasks.size(); j++){
                Task ts = userTasks.get(j);
                if(ts.getdescription().equals(desiredtask.getdescription())){
                    user.removeTask(desiredtask);
                    facade.edit(user);
                }
            }
        }
        taskfacade.remove(desiredtask);
        return Response.status(200).entity("{\"State\":/OK\"}").build();
    }
}
