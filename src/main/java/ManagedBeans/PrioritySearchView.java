/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Facades.AppUserFacade;
import Facades.TaskFacade;
import entities.AppUser;
import entities.Task;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;


/**
 *
 * @author sensez
 */

@ManagedBean(name="PrioritySearchView")
@SessionScoped
public class PrioritySearchView {
    
    
    @EJB
    private TaskFacade TaskFacade = new TaskFacade();
    @EJB
    private AppUserFacade AppFacade = new AppUserFacade();
    
    private EntityManager em;
    private List<Task> tasks = new ArrayList<>();
    private List<Task> temptasks = new ArrayList<>();
    
    private Integer rating1;
    
    public PrioritySearchView(){
    }
    
    public Integer getrating1() {
        return rating1;
    }
 
    public void setrating1(Integer rating) {
        this.rating1 = rating;
    }
    
    public void searchTask(Integer rating1){
        tasks.clear();
        AppUser c = AppFacade.getAppUserByEmail(SessionBean.getUserName());
        temptasks = c.getAllTasks();
        for(int i=0; i<temptasks.size(); i++){
            if(temptasks.get(i).getpriority() == rating1){
                tasks.add(temptasks.get(i));
            }
        }
    }
    
    public List<Task> gettasks(){
        return tasks;
    }
    
    public void settasks(List<Task> tasks){
        this.tasks = tasks;
    }
}
