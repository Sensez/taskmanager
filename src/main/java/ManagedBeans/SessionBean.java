package ManagedBeans;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
 
public class SessionBean {
 
    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
    }
 
    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
    }
 
    public static String getUserName() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        if (session.getAttribute("username") == null)
            return null;
        return session.getAttribute("username").toString();
    }
    
    public static String getUserType(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
           .getExternalContext().getSession(false);
        String type = session.getAttribute("type").toString();
        if (type == null)
            return null;
        return type;
    }
}