/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;
/**
 *
 * @author sensez
 */
import Facades.AppUserFacade;
import Facades.TaskFacade;
import entities.AppUser;
import entities.Task;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
 
@ManagedBean(name="ShareableTaskCreatorView")
@ViewScoped
public class ShareableTaskCreatorView {
     
    
    @EJB
    private TaskFacade TaskFacade = new TaskFacade();
    @EJB
    private AppUserFacade AppFacade = new AppUserFacade();
            
    private String title;
    private String desc;
    private Integer rating1;
    private String users;
 
    public ShareableTaskCreatorView(){
    }
    
    public String getusers(){
        return users;
    }
    
    public void setusers(String users){
        this.users = users;
    }
    
    public String gettitle() {
        return title;
    }
 
    public void settitle(String txt1) {
        this.title = txt1;
    }
 
    public String getdesc() {
        return desc;
    }
 
    public void setdesc(String txt2) {
        this.desc = txt2;
    }
 
    public Integer getrating1() {
        return rating1;
    }
 
    public void setrating1(Integer rating) {
        this.rating1 = rating;
    }
    
    public String registTask(String title, String desc, Integer rating1, String users){
            Task c = new Task(title, desc, rating1);
            c.settitle(title);
            c.setdescription(desc);
            c.setpriority(rating1);
            TaskFacade.create(c);
            String name = SessionBean.getUserName();
            AppUser user = AppFacade.getAppUserByEmail(name);
            user.addTask(c);
            AppFacade.edit(user);
            String parts[] = users.split("\\n");
            String res = "";
            for(String s: parts){
                res = res + " " + s;
            }
            String trimmed = res.trim();
            String partsres[] = res.split("\\s"); 
            for(int i=0; i<partsres.length; i++){
                if(partsres[i].contains("@") && partsres[i].contains(".")){
                    AppUser tempUser = AppFacade.getAppUserByEmail(partsres[i]);
                    tempUser.addTask(c);
                    AppFacade.edit(tempUser);
                }
            }
            return "MainMenu";
    }
}