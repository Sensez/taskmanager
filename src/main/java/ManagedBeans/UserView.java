/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;
import Facades.AppUserFacade;
import entities.AppUser;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;


/**
 *
 * @author sensez
 */


@Named(value = "UserView")
@SessionScoped
public class UserView implements Serializable {

    @EJB
    private AppUserFacade AppUserFacade = new AppUserFacade();

    private String userType;
    
    /**
     * Creates a new instance of UserView
     */
    public UserView() {
    }
    
    public String registUser(String name, String pass, String email, String reppass){
            if(!AppUserFacade.clientExists(email) && reppass.equals(pass)){
                AppUser c = new AppUser(name, email, pass);
                c.setuserName(name);
                c.setemail(email);
                c.setpassword(pass);
                AppUserFacade.create(c);
        }
            
        return "login";
    }

    public AppUserFacade getClientFacade() {
        return AppUserFacade;
    }

    public void setClientFacade(AppUserFacade clientFacade) {
        this.AppUserFacade = clientFacade;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    
    public List<AppUser> getAllClients(){
        return AppUserFacade.findAll();
    }
    
    public void deleteClient(long id){
        AppUserFacade.remove(AppUserFacade.find(id));
    }
 
}