/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;


import Facades.AppUserFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.HttpSession;

@ManagedBean(name="LoginView")
@SessionScoped
public class LoginView implements Serializable {

    private static final long serialVersionUID = 1L;
    private String user;
    private String password;
   
    @EJB
    private AppUserFacade AppUserFacade = new AppUserFacade();
    /**
     * Creates a new instance of LoginView
     */

    public String loginProject() {
        
        System.out.println(this.getUser() + "  " + this.getPassword());

        if (AppUserFacade.validLogin(this.getUser(), this.getPassword())) {

            HttpSession session = SessionBean.getSession();
            session.setAttribute("username", this.getUser());
            session.setAttribute("type", "user");
            System.out.println(SessionBean.getUserType());
            return "MainMenu";
        }
        
    return "login";
    }

    public String getLoggedUser(){
        
        if (SessionBean.getUserName() == null)
            return "";
        
        return SessionBean.getUserName();
    }
    
    public boolean isUserLoggedIn(){
        return !getLoggedUser().equals("");
    }
    
    public String logout() {
        SessionBean.getSession().invalidate();
        return "login";
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
   
    public String getUserType(){
        
        return SessionBean.getUserType();
    }

}
