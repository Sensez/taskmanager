/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Facades.AppUserFacade;
import Facades.TaskFacade;
import entities.AppUser;
import entities.Task;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sensez
 */

@ManagedBean(name = "dataScrollerViewPrioratize")
@ViewScoped

public class dataScrollerViewPrioratize  implements Serializable{
    @PersistenceContext(unitName = "percistenceUnit")
    private EntityManager em;
    private List<Task> tasks;
    private List<Task> toBeDeleted;
    private int ctr = 0; 
    private Task tasktemp;
    private List<Task> tasksSorted;
    private Integer[] priorities;
    private Task selectedTask;
         
    
    @EJB
    private TaskFacade TaskFacade = new TaskFacade();
    
    @EJB
    private AppUserFacade UserFacade = new AppUserFacade();
    
    
    @PostConstruct
    public void init() {
        AppUser user = UserFacade.getAppUserByEmail(SessionBean.getUserName());
        tasks = user.getAllTasks();
        tasksSorted = new ArrayList<Task>();
        toBeDeleted = new ArrayList<Task>();
        priorities = new Integer[tasks.size()];
        
        for(int i=0; i<tasks.size(); i++){
            priorities[i] = tasks.get(i).getpriority();
        }
        Arrays.sort(priorities);
        int ctr = priorities.length-1;
        do{
            for(int i=0; i<tasks.size(); i++){
                if(tasks.get(i).getpriority() == priorities[ctr]){
                    tasksSorted.add(tasks.get(i));
                    tasks.remove(i);
                    ctr--;
                    i=tasks.size()-1;
                }
            }
        }while(tasks.size()!=0);
        tasks = tasksSorted;
    }
    
    public List<Task> gettasks() {
        return tasks;
    }
    
    public Task getSelectedTask() {
        return selectedTask;
    }
    
    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
        AppUser user = UserFacade.getAppUserByEmail(SessionBean.getUserName());
        user.removeTask(selectedTask);
        UserFacade.edit(user);
        TaskFacade.remove(selectedTask);
    }
}
