/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facades;

import entities.AppUser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sensez
 */
@Stateless
public class AppUserFacade extends AbstractFacade<AppUser> {

    @PersistenceContext(unitName = "percistenceUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AppUserFacade() {
        super(AppUser.class);
    }
    
    public boolean validLogin(String email, String password) {
        
        if(clientExists(email)){
            Query query = em.createQuery("SELECT c FROM AppUser c WHERE c.email = '" + email + "' "
                    + "AND c.password = '"+ password +"'");
            AppUser c = (AppUser) query.getSingleResult();

            if (password.equals(c.getpassword())){
                System.out.println("logged");
                return true;
            }
            return false;
        }     
        return false;    
    }
    
    public boolean clientExists(String email){
        Query query = em.createQuery("SELECT c FROM AppUser c WHERE c.email = '" + email + "'");
        List<AppUser> c = query.getResultList();
        return !c.isEmpty();
    }
    
    public AppUser getAppUserByEmail(String username){
        Query query = em.createQuery("SELECT c FROM AppUser c WHERE c.email = '" + username + "'");
        List<AppUser> c = query.getResultList();
        AppUser user = c.get(0);
        return user;
    }
}
