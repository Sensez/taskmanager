/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facades;

import entities.Task;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author sensez
 */
@Stateless
public class TaskFacade extends AbstractFacade<Task> {

    @PersistenceContext(unitName = "percistenceUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TaskFacade() {
        super(Task.class);
    }
    
    public boolean taskExists(String title){
        Query query = em.createQuery("SELECT c FROM Task c WHERE c.title = '" + title + "'");
        List<Task> c = query.getResultList();
        return !c.isEmpty();
    }  
    
    public Task getTaskByTitle(String title){
        Query query = em.createQuery("SELECT c FROM Task c WHERE c.title = '" + title + "'");
        List<Task> c = query.getResultList();
        return c.get(0);
    }
}
